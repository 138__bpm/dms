FROM node:10.15.0

WORKDIR /dms

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
