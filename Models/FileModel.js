const mongoose = require('mongoose');

let File = new mongoose.Schema({
    name: {type: String, required: true},
    content: {type: String},
    userId: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
    folderId: {type: mongoose.Schema.Types.ObjectId, ref: "Folder"},
});

module.exports = mongoose.model('File', File);
