const mongoose = require('mongoose');

let Folder = new mongoose.Schema({
    name: {type: String, required: true, unique: true},
    userId: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
});

module.exports = mongoose.model('Folder', Folder);
