const mongoose = require('mongoose');

let User = new mongoose.Schema({
    userName: {type: String, required: true, unique: true},
    password: {type: String, required: true}
});

module.exports = mongoose.model('User', User);
