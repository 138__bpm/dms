const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('config');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const loginRouter = require('./routes/login');
const fileRouter = require('./routes/file');
const folderRouter = require('./routes/folder');
const auth = require('./middleware/auth');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/login', loginRouter);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use(auth);
app.use('/file', fileRouter);
app.use('/folder', folderRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  return res.status(err.status).send({ "errorMessage": err.message });
});

//For ease of setup for code reviewer, taking db url and credentials from config file instead of environment variables
mongoose.connect(config.db.url)
    .then(() => console.log("Connected to Db"))
    .catch(err => console.log(`Error connecting ${err}`));

module.exports = app;
