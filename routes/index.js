const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('../Models/UserModel');
const File = require('../Models/FileModel');
const Folder = require('../Models/FolderModel');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let user = await User.findById(jwt.decode(req.header('Authorization')).userId);
  let files = await File.find({
    userId: user._id,
    folderId: null
  });
  let folders = await Folder.find({
    userId: user._id
  });
  Array.prototype.push.apply(files,folders);
  res.send(files);
});

module.exports = router;
