const express = require('express');
const jwt = require('jsonwebtoken');
const httpError = require('http-errors');
const router = express.Router();
const File = require('../Models/FileModel');
const User = require('../Models/UserModel');
const Folder = require('../Models/FolderModel');

router.post('/', async function (req, res, next) {
    let fileName = req.body.name;
    let content = req.body.content;
    let folderId = req.body.folderId;
    let user = await User.findById(jwt.decode(req.header('Authorization')).userId);
    let existingFileWithSameName = await File.find({
        name: fileName
    });
    if(existingFileWithSameName.length > 0) {
        return next(httpError(400, 'File Name Should be unique'));
    }
    let file = new File({
        name: fileName,
        content: content,
        userId: user._id,
        folderId: folderId || null
    });
    let response = await file.save();
    res.send(response);
});

router.get('/all/:folderId', async function (req, res, next) {
    let folderId = req.params.folderId;
    let folder = await Folder.findById(folderId);
    if(!folder) {
        return next(httpError(400, 'Bad Request'));
    }
    let files = await File.find({
        folderId: folderId
    });
    res.send(files)
});
module.exports = router;
