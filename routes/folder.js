const express = require('express');
const jwt = require('jsonwebtoken');
const httpError = require('http-errors');
const mongoose = require('mongoose');
const router = express.Router();
const Folder = require('../Models/FolderModel');
const User = require('../Models/UserModel');
const File = require('../Models/FileModel');

router.post('/', async function (req, res, next) {
    let name = req.body.name;
    let user = await User.findById(jwt.decode(req.header('Authorization')).userId);
    let folder = new Folder({
        name: name,
        userId: user._id,
    });
    let response = await folder.save().catch(e => {
        if(e.code === 11000) {
            res.status(400).send({"message": "Folder name should be unique"})
        }
    });
    res.send(response);
});

router.post('/moveFiles/:destinationFolderId', async function (req, res, next) {
    let fileIds = Array.from(req.body.fileIds);
    let destinationFolderId = req.params.destinationFolderId;
    let user = await User.findById(jwt.decode(req.header('Authorization')).userId);
    let destinationFolder = await Folder.find({
        _id: destinationFolderId,
        userId: user._id
    });
    if(destinationFolder.length === 0) {
        return next(httpError(400, 'Bad Data'));
    }
    const session = await mongoose.startSession();
    session.startTransaction();
    for (const fileId of fileIds) {
        let file = await File.findById(fileId);
        if(!file || !file.userId.equals(user._id)) {
            await session.abortTransaction();
            res.send("Invalid data");
            return;
        }
        let existingFileWithSameName = await File.find({
           folderId: destinationFolderId,
           name: file.name
        });
        if(existingFileWithSameName.length > 0) {
            return next(httpError(400, 'Destination Folder already has a file with same name'));
        }
        file.folderId = destinationFolderId;
        await file.save();
    }
    await session.commitTransaction();
    session.endSession();
    res.send("OK");
});
module.exports = router;
