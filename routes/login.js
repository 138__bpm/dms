const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const httpError = require('http-errors');
const router = express.Router();
const UserModel = require('../Models/UserModel');
const config = require('config');
router.post('/', async function (req, res, next) {
    let userName = req.body.userName;
    let password = req.body.password;
    let user = await UserModel.find({
        userName: userName
    });
    if (!user || !await bcrypt.compare(password, user[0].password)) {
        return next(httpError(400, 'Bad Request'));
    }
    const token = jwt.sign({userId: user[0]._id}, config.jwtPrivateKey, {expiresIn: '1h'});
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({"token": token});

});
module.exports = router;
