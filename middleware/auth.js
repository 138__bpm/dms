const jwt = require('jsonwebtoken');
const config = require('config');
function auth(req, res, next) {
    const token = req.header('Authorization');
    if(!token) return res.status(401).send('Access denied. No token provided');

    try {
        jwt.verify(token, config.jwtPrivateKey);
        // req.app.locals.userId = jwt.decode(token).user;
        next();
    } catch (ex) {
        res.status(400).send('Bad Request hh');
    }
}

module.exports = auth;
